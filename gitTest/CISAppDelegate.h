//
//  CISAppDelegate.h
//  gitTest
//
//  Created by Алексей Цысс on 31.07.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CISAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
