//
//  main.m
//  gitTest
//
//  Created by Алексей Цысс on 31.07.14.
//  Copyright (c) 2014 aetsyss. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CISAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CISAppDelegate class]));
    }
}
